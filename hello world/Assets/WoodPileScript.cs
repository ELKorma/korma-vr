using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodPileScript : MonoBehaviour
 
{
    public GameObject woodPrefab; // Prefab of the wood object to spawn
    public Transform spawnPoint; // Transform of the spawn point for the wood object

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Axe"))
        {
            // Spawn a piece of wood at the spawn point
            Instantiate(woodPrefab, spawnPoint.position, spawnPoint.rotation);
        }
    }
}