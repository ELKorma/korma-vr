using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class animateRH : MonoBehaviour
{
    public InputActionProperty PinchAnimationAction;
    public Animator handAnimator;
    public InputActionProperty GripAnimationAction;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float triggerValue = PinchAnimationAction.action.ReadValue<float>();
        handAnimator.SetFloat("Trigger", triggerValue);
        float GripValue = GripAnimationAction.action.ReadValue<float>();
        handAnimator.SetFloat("Grip", GripValue);
    }
}
