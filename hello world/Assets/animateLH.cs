using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class animateLH : MonoBehaviour
{
    public InputActionProperty PinchAnimationAction;
    // Start is called before the first frame update
    public Animator handAnimator;
    public InputActionProperty GripAnimationAction;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
     float triggerValue = PinchAnimationAction.action.ReadValue<float>();

        handAnimator.SetFloat("Trigger", triggerValue);
        float GripValue = GripAnimationAction.action.ReadValue<float>();
        handAnimator.SetFloat("Grip", GripValue);




    }
}
