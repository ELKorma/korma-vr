using UnityEngine;

public class Flint : MonoBehaviour
{
    public ParticleSystem particleSystem;
    public GameObject otherObject;
    public int collisionCount = 3;
    public Vector3 particlePosition;

    private int collisions = 0;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == otherObject)
        {
            collisions++;

            if (collisions >= collisionCount)
            {
                // Spawn the particle system at the specified position
                ParticleSystem newParticleSystem = Instantiate(particleSystem, particlePosition, Quaternion.identity);

                // Reset the collision count
                collisions = 0;
            }
        }
    }
}
