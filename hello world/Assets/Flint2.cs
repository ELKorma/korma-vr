using UnityEngine;

public class Flint2 : MonoBehaviour
{
    public GameObject otherObject;
    public ParticleSystem particleEffect;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == otherObject)
        {
            if (particleEffect != null)
            {
                ParticleSystem newParticleSystem = Instantiate(particleEffect, transform.position, Quaternion.identity);
            }
        }
    }
}