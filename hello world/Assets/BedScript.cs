using UnityEngine;

public class BedScript : MonoBehaviour
{
    public GameObject player;
    public Transform lookAtTarget;
    public Light directionalLight;
    public float timeScale = 10f;
    public float transitionDuration = 10f;
    public AnimationCurve curve;
    public float delay = 4f;

    private bool inBed = false;
    private Quaternion initialRotation;
    private float initialTime;
    private float endTime;
    private float delayEndTime;

    void Start()
    {
        initialRotation = directionalLight.transform.rotation;
        delayEndTime = Time.time + delay;
    }

    void Update()
    {
        if (inBed)
        {
            float t = (Time.time - initialTime) / transitionDuration;
            directionalLight.transform.rotation = Quaternion.Lerp(initialRotation, initialRotation * Quaternion.Euler(180f, 0f, 0f), curve.Evaluate(t));

            if (t >= 1f)
            {
                // Reset the light rotation and time scale
                directionalLight.transform.rotation = initialRotation;
                Time.timeScale = 1f;

                // Exit the bed
                inBed = false;
                player.transform.position = transform.position + transform.forward * 2f;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player && Time.time >= delayEndTime)
        {
            // Look at the sky
            player.transform.LookAt(lookAtTarget);

            // Start the time lapse
            inBed = true;
            initialTime = Time.time;
            endTime = initialTime + transitionDuration;
            Time.timeScale = timeScale;
        }
    }
}
