using UnityEngine;

public class WoodPile : MonoBehaviour
{

    public GameObject woodPrefab;
    public AudioClip woodSound;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Hands")
        {
            // Play wood sound
            AudioSource.PlayClipAtPoint(woodSound, transform.position);

            // Spawn wood object
            Instantiate(woodPrefab, transform.position, Quaternion.identity);

            // Reset wood pile collider
            GetComponent<Collider>().isTrigger = false;
        }
    }
}
