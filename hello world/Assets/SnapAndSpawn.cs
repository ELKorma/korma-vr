using UnityEngine;

public class SnapAndSpawn : MonoBehaviour
{
    public Transform snapPosition;
    public float snapDistance = 0.5f;
    public AudioClip snapSound;
    public GameObject newObjectPrefab;

    private bool isSnapped = false;

    private void OnTriggerEnter(Collider other)
    {
        // Check if the other object is close enough to the snap position
        if (Vector3.Distance(other.transform.position, snapPosition.position) < snapDistance)
        {
            // Snap the object to the snap position
            transform.position = snapPosition.position;
            transform.rotation = snapPosition.rotation;

            // Disable the object's rigidbody to prevent it from moving
            GetComponent<Rigidbody>().isKinematic = true;

            // Play a snap sound effect
            AudioSource.PlayClipAtPoint(snapSound, transform.position);

            isSnapped = true;

            // Destroy this object
            Destroy(gameObject);

            // Spawn a new object at the snap position
            Instantiate(newObjectPrefab, snapPosition.position, snapPosition.rotation);
        }
    }
}
