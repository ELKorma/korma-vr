using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class TreeChopper : MonoBehaviour
{

    public int numChopsRequired = 3;
    public GameObject woodPile;

    private int numChopsReceived = 0;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Axe")
        {
            numChopsReceived++;
            if (numChopsReceived >= numChopsRequired)
            {
                // Replace the tree with a wood pile
                Instantiate(woodPile, transform.position, transform.rotation);
                Destroy(gameObject);
            }
        }
    }
}
